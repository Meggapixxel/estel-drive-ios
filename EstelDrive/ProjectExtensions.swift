import UIKit
import Foundation

extension UIButton {
    
    @IBInspectable var shadow: Bool {
        get { return layer.shadowOpacity > 0.0 }
        set { if newValue == true { self.addShadow() } }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get { return self.layer.cornerRadius }
        set {
            self.layer.cornerRadius = newValue
            if shadow == false { self.layer.masksToBounds = true }
        }
    }

    
    @IBInspectable var borderWidth: CGFloat {
        get { return self.layer.borderWidth }
        set { self.layer.borderWidth = newValue }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            if let borderColor = self.layer.borderColor { return UIColor(cgColor: borderColor) }
            else { return UIColor.black }
        }
        set { self.layer.borderColor = newValue.cgColor }
    }
    
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}
extension Int {

    func formatted() -> String { return self < 10 ? "0\(self)" : "\(self)" }
    
    var filesRightWord: String {
        switch self {
        case 1:
            return "файл"
        case 2:
            return "файла"
        default:
            return "файлов"
        }
    }
}
extension Bundle {
    
    var releaseVersionNumber: String {
        if let version = infoDictionary?["CFBundleShortVersionString"] as? String {
            return "Версия программы: \(version)"
        }
        return "Не удалось отобразить версию программы"
    }
}
extension String {
    
    var hex: Int? { return Int(self, radix: 16) }
    
    var fileExt: String {
        let array = self.components(separatedBy: ".")
        return array.count < 2 ? "" : array.last!.lowercased()
    }
    
    var fileName: String? {
        let array = self.components(separatedBy: "/")
        return array.count == 0 ? nil : array.last
    }
    
    func firstUppercased() -> String {
        var chars = self.characters.map { return String($0) }
        if chars.count > 0 {
            chars[0] = chars[0].uppercased()
            return chars.joined()
        }
        return ""
    }
    
    func pathFirstUppercased() -> String {
        let editedPath = String(self.characters.dropFirst())
        let components = editedPath.components(separatedBy: "/")
        return "/" + components.map { return $0.firstUppercased() }.joined(separator: "/")
    }
    
    func folderTreeFrom() -> [String] {
        let editedPath = String(self.characters.dropFirst())
        let components = editedPath.components(separatedBy: "/")
        return components.map { return $0.firstUppercased() }
    }
}
extension UIColor {
    
    convenience init(hex: Int) {
        self.init(hex: hex, a: 1.0)
    }
    
    convenience init(hex: Int, a: CGFloat) {
        self.init(r: (hex >> 16) & 0xff, g: (hex >> 8) & 0xff, b: hex & 0xff, a: a)
    }
    
    convenience init(r: Int, g: Int, b: Int) {
        self.init(r: r, g: g, b: b, a: 1.0)
    }
    
    convenience init(r: Int, g: Int, b: Int, a: CGFloat) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: a)
    }
    
    convenience init?(hexString: String) {
        guard let hex = hexString.hex else {
            return nil
        }
        self.init(hex: hex)
    }
}
extension Array {
    
    var foldersPath: String? {
        if let array = self as? [String], self.count > 1 {
            return array.dropLast().joined(separator: "/")
        }
        return nil
    }
}
extension Date {

    var hhmmddMMyyyy: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm dd-MM-yyyy"
        return formatter.string(from: self)
    }
    
    var ddMMyyyy: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter.string(from: self)
    }
}
extension UInt64 {
    var formatted: String {
        guard self != 0 else { return "0 байт" }
        var convertedValue: Double = Double(self)
        var multiplyFactor = 0
        let tokens = ["байт", "кб", "мб", "гб"]
        while convertedValue > 1024 {
            convertedValue /= 1024
            multiplyFactor += 1
        }
        return String(format: "%4.2f %@", convertedValue, tokens[multiplyFactor])
    }
}

