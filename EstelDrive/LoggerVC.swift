import UIKit
import RealmSwift

class LoggerVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var logProvider = LogProvider()
    let settings = Settings()
    var logs = [LogItem]()
    var realm: Realm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 66
        realm = try! Realm()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if settings.restoreHasAccess() {
            logs = logProvider.updateLogs().reversed()
            tableView.reloadData()
        } else {
            logs.removeAll()
        }
    }
}
extension LoggerVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventLogCell", for: indexPath) as! EventLogCell
        let logItem = logs[indexPath.row]
        cell.logFile.text = logItem.logFile
        cell.logDate.text = logItem.logDate?.hhmmddMMyyyy
        cell.logStatus.text = logItem.logStatus ? "Успешно" : "Ошибка"
        cell.logStatus.textColor = logItem.logStatus ? UIColor.green : UIColor.red
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return logs.count
    }
}
