import Foundation

class Settings: NSObject {
    
    private let userDefaults = Foundation.UserDefaults.standard
    private let USER_ID = "USERID"
    private let SCHEDULED = "SCHEDULED"
    private let USER_NAME = "USERNAME"
    private let LOGGED_IN = "LOGGEDIN"
    private let HAS_ACCESS = "HASACCESS"
    private let SCHEDULED_TIME = "SCHEDULEDTIME"
    private let LAST_TIME_CHECKED = "LASTTIMECHECKED"
        
    func saveUserId(userId: String) {
        userDefaults.set(userId, forKey: USER_ID)
        saveLoggedIn(status: true)
    }
    
    func restoreUserId() -> String? {
        return userDefaults.string(forKey: USER_ID)
    }
    
    func saveUserName(userName: String = "Вы еще не авторизовались") {
        userDefaults.set(userName, forKey: USER_NAME)
    }
    
    func restoreUserName() -> String {
        return userDefaults.string(forKey: USER_NAME) ?? "Вы еще не авторизовались"
    }
    
    func saveScheduledTime(time: String) {
        userDefaults.set(time, forKey: SCHEDULED_TIME)
    }
    
    func restoreScheduledTime() -> String {
        return userDefaults.string(forKey: SCHEDULED_TIME) ?? "20:00"
    }
    
    func saveLastTimeChecked() {
        userDefaults.set(Date().ddMMyyyy, forKey: LAST_TIME_CHECKED)
    }
    
    func restoreLastTimeChecked() -> String {
        return userDefaults.string(forKey: LAST_TIME_CHECKED) ?? "Не проверялось"
    }
    
    func saveLoggedIn(status: Bool) {
        userDefaults.set(status, forKey: LOGGED_IN)
    }
    
    func restoreLoggedIn() -> Bool {
        return userDefaults.bool(forKey: LOGGED_IN)
    }
    
    func saveHasAccess(status: Bool) {
        userDefaults.set(status, forKey: HAS_ACCESS)
    }
    
    func restoreHasAccess() -> Bool {
        return userDefaults.bool(forKey: HAS_ACCESS)
    }
    
    func saveScheduled(status: Bool) {
        userDefaults.set(status, forKey: SCHEDULED)
    }
    
    func restoreScheduled() -> Bool {
        return userDefaults.bool(forKey: SCHEDULED)
    }
    
    func oneTimeSchedule() -> Bool {
        return false
    }
    
    func logOut() {
        saveUserId(userId: "")
        saveLoggedIn(status: false)
        saveHasAccess(status: false)
        saveScheduled(status: false)
        saveUserName()
        saveScheduled(status: false)
    }
}
