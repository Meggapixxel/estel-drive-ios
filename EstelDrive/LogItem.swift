import Realm
import RealmSwift

class LogItem: Object {

    dynamic var logFile: String? = nil
    dynamic var logDate: Date? = nil
    dynamic var logStatus: Bool = false
    
}
