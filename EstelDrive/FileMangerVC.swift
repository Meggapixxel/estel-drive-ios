import UIKit
import QuickLook
import SVProgressHUD
import AVKit
import AVFoundation

class FileMangerVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pathScrollView: UIScrollView!
    @IBOutlet weak var scrollablePath: UILabel!
    
    let settings = Settings()
    let fileManager  = FileProvider()
    
    var fileFolders = [FileFolder]()
    var searchResult = [FileFolder]()
    var searchActive = false
    var fileURL: NSURL!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 66
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if settings.restoreHasAccess() {
            fileFolders = fileManager.getCurrentFileFolders()
            proccessPath()
        } else {
            fileFolders.removeAll()
        }
    }
    
    func proccessPath() {
        scrollablePath.text = fileManager.currentPathWithoutRoot()
        scrollablePath.sizeToFit()
        scrollablePath.sizeThatFits(CGSize(width: scrollablePath.frame.width, height: pathScrollView.frame.height))
        pathScrollView.contentSize = CGSize(width: scrollablePath.frame.width, height: pathScrollView.frame.height)
        tableView.reloadData()
    }
}
extension FileMangerVC: QLPreviewControllerDataSource {

    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }

    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return fileURL
    }
}
extension FileMangerVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filefoldertvc", for: indexPath) as! FileFolderTVC
        let model = searchActive ? searchResult[indexPath.row] : fileFolders[indexPath.row]
        cell.icon.image = model.image
        cell.title.text = model.name
        cell.date.text = model.date
        cell.size.text = model.size
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchActive ? searchResult.count : fileFolders.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fileFolder = searchActive ? searchResult[indexPath.row] : fileFolders[indexPath.row]
        if fileFolder.selfFolder != nil || fileFolder.root != nil {
            fileFolders = fileManager.fileFolders(folder: fileFolder)
            proccessPath()
        } else if let file = fileFolder.selfFile {
            switch fileFolder.name.fileExt {
            case "mov", "avi", "mp4":
                let player = AVPlayer(url: URL(fileURLWithPath: file.path))
                let playerController = AVPlayerViewController()
                playerController.player = player
                present(playerController, animated: true) { player.play() }
                break
            default:
                fileURL = NSURL(fileURLWithPath: file.path)
                if QLPreviewController.canPreview(fileURL!) {
                    let quickLookController = QLPreviewController()
                    quickLookController.dataSource = self
                    quickLookController.currentPreviewItemIndex = 1
                    navigationController?.pushViewController(quickLookController, animated: true)
                }
                break
            }
        } else {
            let alertController = UIAlertController(title: "Ошибка", message: "Невозможно определить формат файла", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            let fileFolder = fileFolders[indexPath.row]
            if fileFolder.selfFile != nil {
                fileManager.delete(name: fileFolder.name)
            } else if let folder = fileFolder.selfFolder {
                fileManager.delete(folder: folder)
            }
            fileFolders.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
}
extension FileMangerVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchNameOk = searchBar.text {
            searchBar.setShowsCancelButton(true, animated: true)
            searchActive = true
            searchResult = fileManager.searchForFile(name: searchNameOk)
            tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.endEditing(true)
        searchBar.text = ""
        searchActive = false
        searchResult.removeAll()
        tableView.reloadData()
    }
}
