import Foundation

class FileProvider: NSObject {

    private let DROPBOX = "Dropbox"
    private let DOCUMENTS = "Documents"
    private let fileSystem = FileSystem()
    private let logProvider = LogProvider()
    private lazy var currentFolder: Folder? = self.getDropboxFolder()
    private var stackFolders = [FileFolder]()
    lazy var files: [String] = self.getAllFiles()
    
    func homeFolderExists() -> Bool {
        return getDropboxFolder() != nil
    }
    
    func getDropboxFolder() -> Folder? {
        let homeFolder = fileSystem.homeFolder
        if homeFolder.containsSubfolder(named: DOCUMENTS), let documents = try? homeFolder.subfolder(named: DOCUMENTS) {
            if documents.containsSubfolder(named: DROPBOX) {
                return try? documents.subfolder(named: DROPBOX)
            } else if let dropboxFolder = try? documents.createSubfolder(named: DROPBOX) {
                return dropboxFolder
            }
        } else if let documents = try? homeFolder.createSubfolder(named: DOCUMENTS) {
            if documents.containsSubfolder(named: DROPBOX) {
                return try? documents.subfolder(named: DROPBOX)
            } else if let dropboxFolder = try? documents.createSubfolder(named: DROPBOX) {
                return dropboxFolder
            }
        }
        return nil
    }

    func getCurrentFileFolders() -> [FileFolder] {
        if let currentFolderOk = currentFolder {
            if stackFolders.count > 0 {
                return [stackFolders[stackFolders.count - 1]] + fileFolders(folder: currentFolderOk)
            } else {
                return fileFolders(folder: currentFolderOk)
            }
        }
        return rootFolder()
    }
    
    func rootFolder() -> [FileFolder] {
        stackFolders.removeAll()
        if let folderOk = getDropboxFolder() {
            currentFolder = folderOk
            return fileFolders(folder: folderOk)
        }
        return []
    }
    
    func fileFolders(folder: FileFolder) -> [FileFolder] {
        if let selfFolderOk = folder.selfFolder {
            stackFolders.append(FileFolder(root: currentFolder!))
            currentFolder = selfFolderOk
            return [stackFolders[stackFolders.count - 1]] + fileFolders(folder: selfFolderOk)
        } else if let rootOk = folder.root {
            var folders = [FileFolder]()
            if stackFolders.count == 1 {
                stackFolders.removeLast()
            } else {
                folders.append(stackFolders[stackFolders.count - 2])
                stackFolders.removeLast()
            }
            currentFolder = rootOk
            return folders + fileFolders(folder: rootOk)
        }
        return []
    }
    
    private func fileFolders(folder: Folder) -> [FileFolder] {
        return listFolders(folder: folder) + listFiles(folder: folder)
    }
    
    func listFolders(folder: Folder) -> [FileFolder] {
        var folders = [FileFolder]()
        folder.makeSubfolderSequence(recursive: false).forEach { subFolder in
            folders.append(FileFolder(name: subFolder.name, selfFolder: subFolder))
        }
        return folders
    }
    
    func listFiles(folder: Folder) -> [FileFolder] {
        var files = [FileFolder]()
        folder.makeFileSequence(recursive: false).forEach { file in
            let size = (try! file.size()).formatted
            let date = (try! file.date())
            files.append(FileFolder(name: file.name, date: date, size: size, selfFile: file))
        }
        return files
    }
    
    func getFiles() -> [File] {
        if let currentFolderOk = currentFolder {
            let allFiles = currentFolderOk.makeFileSequence(recursive: true)
            return allFiles.map { return $0 }
        }
        return []
    }
    
    func getAllFiles() -> [String] {
        if let currentFolderOk = currentFolder {
            let allFiles = currentFolderOk.makeFileSequence(recursive: true, includeHidden: false)
            return allFiles.map{ return pathWithoutRoot(path: $0.path, root: currentFolderOk) }
        }
        return []
    }
    
    func pathWithoutRoot(path: String) -> String {
        if let currentFolderOk = currentFolder {
            return path.replacingOccurrences(of: currentFolderOk.path, with: "/")
        }
        return ""
    }
    
    func pathWithoutRoot(path: String, root: Folder) -> String {
        return path.replacingOccurrences(of: root.path, with: "/")
    }
    
    func currentPathWithoutRoot() -> String {
        if let currentFolderOk = currentFolder, let dropboxRange = currentFolderOk.path.range(of: DROPBOX) {
            return currentFolderOk.path.substring(from: dropboxRange.upperBound)
        }
        return "/"
    }
    
    func createFileWith(path: String, data: Data) {
        let foldersTree = path.folderTreeFrom()
        if foldersTree.count == 1, let currentFolderOk = currentFolder {
            writeWithLog(folder: currentFolderOk, fileName: foldersTree[0], data: data)
        } else {
            if let currentFolderOk = currentFolder, let fileName = foldersTree.last {
                if let foldersPath = foldersTree.foldersPath, let folder = try? currentFolderOk.subfolder(atPath: foldersPath) {
                    writeWithLog(folder: folder, fileName: fileName, data: data)
                } else if let folder = createFolders(folders: foldersTree.dropLast(), rootFolder: currentFolderOk) {
                    writeWithLog(folder: folder, fileName: fileName, data: data)
                }
            }
        }
    }
    
    func writeWithLog(folder: Folder, fileName: String, data: Data) {
        if let file = try? folder.createFile(named: fileName) {
            if (try? file.write(data: data)) != nil {
                logProvider.writeLog(logFile: fileName, logStatus: true)
            } else {
                logProvider.writeLog(logFile: fileName)
            }
        } else {
            logProvider.writeLog(logFile: fileName)
        }
    }
    
    func createFolders(folders: ArraySlice<String>, rootFolder: Folder) -> Folder? {
        if let tmp = folders.first, let folder = try? rootFolder.subfolder(named: tmp) {
            return createFolders(folders: folders.dropFirst(), rootFolder: folder)
        } else if let tmp = folders.first, let folder = try? rootFolder.createSubfolder(named: tmp) {
            return createFolders(folders: folders.dropFirst(), rootFolder: folder)
        } else {
            return rootFolder
        }
    }
    
    func delete(filesToDelete: [String]) {
        for file in filesToDelete {
            delete(pathWithoutRoot: file)
        }
    }

    func delete(pathWithoutRoot: String) {
        if let currentFolderOk = currentFolder, let fileToDelete = try? currentFolderOk.file(atPath: pathWithoutRoot) {
            _ = try? fileToDelete.delete()
        }
    }
    
    func delete(folder: Folder) {
        _ = try? folder.delete()
    }
    
    func delete(name: String) {
        if let currentFolderOk = currentFolder, let file = try? currentFolderOk.file(named: name) {
            _ = try? file.delete()
        }
    }
    
    func searchForFile(name: String) -> [FileFolder] {
        var result = [FileFolder]()
        let nameLowercased = name.lowercased()
        if let rootFolder = getDropboxFolder() {
            let allFiles = rootFolder.makeFileSequence(recursive: true, includeHidden: false)
                .map { return $0 }
                .filter { return $0.name.lowercased().range(of: nameLowercased) != nil }
            allFiles.forEach({ (file) in
                result.append(FileFolder(name: file.name, date: try! file.date(), size: (try! file.size()).formatted, selfFile: file))
            })
        }
        return result
    }
}
