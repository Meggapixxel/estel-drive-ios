import Foundation
import SwiftyDropbox

class DropboxApi: NSObject {
    
    private let client = DropboxClient(accessToken: "IK-JwJFwE74AAAAAAAAf3hftOCMHnbKn7NfFV9mbiZ6HAWJbtATt4OlmGhPLRUxc")
    let fileProvider = FileProvider()
    
    func listFilesWith(path: String = "", result: @escaping ((Array<Files.Metadata>?, String?) -> Void)) {
        client.files.listFolder(path: path, recursive: true).response { response, error in
            if let responseOk = response {
                result(responseOk.entries, nil)
            } else if let errorOk = error {
                result(nil, errorOk.description)
            }
        }
    }
    
    
    func downloadFile(path: String, data:  @escaping (Data?) -> Void) {
        client.files.download(path: path)
            .response { response, error in
                if let response = response {
                    data(response.1)
                } else if error != nil {
                    data(nil)
                }
            }
    }
}
