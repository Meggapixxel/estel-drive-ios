import UIKit
import SwiftyDropbox
import SVProgressHUD
import FacebookLogin
import ActionSheetPicker_3_0

class MainVC: UIViewController {

    @IBOutlet weak var fbName: UILabel!
    @IBOutlet weak var scheduledTime: UILabel!
    @IBOutlet weak var lastTimeCheck: UILabel!
    @IBOutlet weak var appVersion: UILabel!
    
    @IBOutlet weak var accessCheck: UIButton!
    @IBOutlet weak var scheduleOn: UIButton!
    @IBOutlet weak var scheduleOff: UIButton!
    @IBOutlet weak var timePicker: UIButton!
    @IBOutlet weak var manualCheck: UIButton!
    
    let settings = Settings()
    let downloadProvider = DownloadProvider()
    lazy var facebook: Facebook = { Facebook(callback: self) }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadEnabled()
    }
    
    @IBAction func didTapLogin(_ sender: Any) {
        let loginManager = LoginManager()
        if settings.restoreLoggedIn() {
            let alertController = UIAlertController(title: "Выход из учетной записи", message: "Вы действительно хотите выйти?", preferredStyle: .alert)
            alertController.addAction( UIAlertAction(title: "Да", style: .destructive, handler: { (alert) in
                loginManager.logOut()
                self.settings.logOut()
                self.didTapCancelSchedule()
            }))
            alertController.addAction(UIAlertAction(title: "Нет", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        } else {
            loginManager.logIn([ .publicProfile ], viewController: self) { loginResult in
                switch loginResult {
                case .failed(let error):
                    SVProgressHUD.showError(withStatus: "Ошибка входа. \(error)")
                    break
                case .cancelled:
                    SVProgressHUD.showError(withStatus: "Отмена входа")
                    break
                case .success( _, _, _):
                    SVProgressHUD.show()
                    self.facebook.getFBinfo()
                    break
                }
            }
        }
    }
    
    @IBAction func didTapCheckAccess(_ sender: Any) {
        if let userId = settings.restoreUserId() {
            SVProgressHUD.show()
            facebook.checkUserInGroupWith(id: userId)
        } else {
            SVProgressHUD.showError(withStatus: "Вы не вошли в аккаунт Facebook")
        }
    }
    
    @IBAction func didTapSchedule() {
        (UIApplication.shared.delegate as! AppDelegate).scheduleNotification(at: Date())
        settings.saveScheduled(status: true)
        reloadEnabled()
    }
    
    @IBAction func didTapCancelSchedule() {
        cancelSchedule()
        reloadEnabled()
    }
    
    @IBAction func didTapTimePicker(_ sender: Any) {
        ActionSheetMultipleStringPicker.show(withTitle: "Время проверки", rows: [
            (0...23).map { $0.formatted() },
            [":"] ,
            (0...59).map { $0.formatted() }
            ], initialSelection: [13, 0, 0], doneBlock: {
                picker, indexes, values in
                let time = (values as! [String]).joined()
                self.settings.saveScheduledTime(time: time)
                self.scheduledTime.text = time
                self.didTapCancelSchedule()
                return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func didTapManualCheck(_ sender: Any) {
        SVProgressHUD.show()
        downloadProvider.searchForAllFiles { (tuple, error) in
            if let tupleOk = tuple, error == nil {
                self.reloadEnabled()
                SVProgressHUD.dismiss()
                self.present(self.resultAlert(tuple: tupleOk), animated: true, completion: nil)
            } else if let errorOk = error {
                SVProgressHUD.showError(withStatus: errorOk)
            }
        }
    }

    func reloadEnabled() {
        fbName.text = settings.restoreUserName()
        scheduledTime.text = settings.restoreScheduledTime()
        lastTimeCheck.text = settings.restoreLastTimeChecked()
        appVersion.text = Bundle.main.releaseVersionNumber
        
        accessCheck.isEnabled = settings.restoreLoggedIn()
        accessCheck.backgroundColor = accessCheck.isEnabled ? UIColor(hexString: "416BC1") : UIColor.gray
        scheduleOn.isEnabled = !settings.restoreScheduled() && settings.restoreHasAccess()
        scheduleOn.backgroundColor = scheduleOn.isEnabled ? UIColor(hexString: "416BC1") : UIColor.gray
        scheduleOff.isEnabled = !scheduleOn.isEnabled && settings.restoreHasAccess()
        scheduleOff.backgroundColor = scheduleOff.isEnabled ? UIColor(hexString: "416BC1") : UIColor.gray
        timePicker.isEnabled = settings.restoreHasAccess()
        manualCheck.isEnabled = settings.restoreHasAccess()
    }
    
    func cancelSchedule() {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.cancelSchedule()
            settings.saveScheduled(status: false)
        }
    }
}
extension MainVC {

    func resultAlert(tuple: (Int, String)) -> UIAlertController {
        let message = tuple.0 == 0 ? "Нет новых файлов для скачивания" : "Доступно \(tuple.0) новых файлов в размере \(tuple.1)"
        let alertController = UIAlertController(title: "Результат проверки", message: message, preferredStyle: .alert)
        if tuple.0 != 0 {
            alertController.addAction( UIAlertAction(title: "Скачать", style: .default, handler: { (alert) in
                (UIApplication.shared.delegate as! AppDelegate).doBackgroundTask()
            }))
            alertController.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
        } else {
            alertController.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
        }
        return alertController
    }
}
extension MainVC: FBCallback {
    
    func didGotFBinfo(info: MyProfileRequest.Response?) {
        if let infoOk = info, let dictOk = infoOk.dictionaryValue,
            let nameOk = dictOk["name"] as? String, let idOk = dictOk["id"] as? String {
            SVProgressHUD.showSuccess(withStatus: "Успешно")
            settings.saveUserName(userName: nameOk)
            settings.saveUserId(userId: idOk)
            reloadEnabled()
        } else {
            SVProgressHUD.showError(withStatus: "Не удалось получить ваши данные. Попробуйте позже.")
        }
    }
    
    func didGotFBAccess(status: Bool?) {
        if let statusOk = status {
            settings.saveHasAccess(status: statusOk)
            reloadEnabled()
            if statusOk {
                SVProgressHUD.showSuccess(withStatus: "Успешно")
            } else {
                cancelSchedule()
                SVProgressHUD.showError(withStatus: "Вы не состоите в группе. Вступите в группу и попробуйте еще раз")
            }
        } else {
            cancelSchedule()
            SVProgressHUD.showError(withStatus: "Ошибка получения данных")
        }
    }
}
