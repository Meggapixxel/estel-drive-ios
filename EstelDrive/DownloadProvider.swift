import Foundation
import SystemConfiguration
import SVProgressHUD
import SwiftyDropbox

class DownloadProvider: NSObject {

    let dropboxApi = DropboxApi()
    let facebook = Facebook()
    let fileProvider = FileProvider()
    let logProvider = LogProvider()
    let settings = Settings()
    var downloadDesciption = ""
    var downloadedFiles = 0 { didSet { progress() } }
    var downloadStatus = true
    static var newFiles: [Files.FileMetadata] = []
    
    func searchForAllFiles(newFilesCount: @escaping (((Int, String)?, String?) -> Void)) {
        facebook.checkUserInGroupWith(id: settings.restoreUserId()) { (status) in
            if let statusNonNil = status {
                if statusNonNil {
                    if self.fileProvider.homeFolderExists() {
                        self.dropboxApi.listFilesWith(path: "") { (objects, error) in
                            if let objectsOk = objects {
                                self.settings.saveLastTimeChecked()
                                let onlineFiles = objectsOk.filter { $0 is Files.FileMetadata } as! [Files.FileMetadata]
                                newFilesCount(self.findNewFiles(onlineFiles: onlineFiles), nil)
                            } else if let errorOk = error {
                                newFilesCount(nil, "Не удалось получить список файлов. Ошибка: \(errorOk) ")
                            }
                        }
                    } else {
                        newFilesCount(nil, "Не удалось создать директорию для скачивания")
                    }
                } else {
                    newFilesCount(nil, "Нет доступа к скачиванию")
                }
            } else {
                newFilesCount(nil, "Не удалось проверить доступ к скачиванию")
            }
        }
    }
    
    func findNewFiles(onlineFiles: [Files.FileMetadata]) -> (Int, String) {
        let offlineFiles = fileProvider.getFiles()
        let onlineFilesFiltered = onlineFiles.filter { $0.pathDisplay != nil }
        
        
        let offlineFilesNames = fileProvider.getAllFiles()
        let onlineFilesNames = onlineFilesFiltered.map { $0.pathDisplay!.pathFirstUppercased() }
        
        
        let offlineFilesNamesSet = Set(offlineFilesNames)
        let onlineFilesNamesSet = Set(onlineFilesNames)
        
        
        let filesToDelete = Array(offlineFilesNamesSet.subtracting(onlineFilesNamesSet))
        fileProvider.delete(filesToDelete: filesToDelete)
        
        
        let newFiles = onlineFilesNamesSet.subtracting(offlineFilesNamesSet)
        DownloadProvider.newFiles = onlineFilesFiltered.filter { newFiles.contains($0.pathDisplay!.pathFirstUppercased()) }
        
        
        let intersaction = onlineFilesNamesSet.intersection(offlineFilesNamesSet)
        for filePath in intersaction {
            let offFile = offlineFiles.first(where: { fileProvider.pathWithoutRoot(path: $0.path) == filePath} )!
            let onFile = onlineFilesFiltered.first(where: { $0.pathDisplay!.pathFirstUppercased() == filePath} )!
            let offDate = try? offFile.date()
            let onDate = onFile.serverModified
            if let offDateOk = offDate, offDateOk < onDate {
                DownloadProvider.newFiles.append(onFile)
            }
        }
        
        var sizeToDownload: UInt64 = 0
        DownloadProvider.newFiles.forEach { sizeToDownload += $0.size }
        return (DownloadProvider.newFiles.count, sizeToDownload.formatted)
    }
    
    func downloadFile(files: ArraySlice<Files.FileMetadata>, group: DispatchGroup) {
        if isInternetConnectionActive() {
            if fileProvider.homeFolderExists() {
                if files.count > 0, let currentFile = files.first, let pathLower = currentFile.pathLower, let displayPath = currentFile.pathDisplay {
                    self.dropboxApi.downloadFile(path: pathLower, data: { data in
                        if let dataOk = data {
                            self.downloadedFiles += 1
                            self.fileProvider.createFileWith(path: displayPath, data: dataOk)
                            self.downloadFile(files: files.dropFirst(), group: group)
                        } else {
                            self.downloadDesciption = "Ошибка скачивания файла. Скачано \(self.downloadedFiles) \(self.downloadedFiles.filesRightWord)"
                            self.downloadStatus = false
                            self.logProvider.writeLog(logFile: currentFile.name)
                            group.leave()
                        }
                    })
                } else {
                    downloadDesciption = "Скачано \(self.downloadedFiles) \(self.downloadedFiles.filesRightWord)"
                    group.leave()
                }
            } else {
                downloadDesciption = "Не удалось создать диреторию для скачивания"
                group.leave()
            }
        } else {
            downloadDesciption =  "Нет доступа к интернету. Скачано \(downloadedFiles) \(self.downloadedFiles.filesRightWord)"
            self.downloadStatus = false
            group.leave()
        }
    }
    
    func downloadNewFilesSync(result: @escaping ((String, Bool) -> Void)) {
        downloadedFiles = 0
        let group = DispatchGroup()
        group.enter()
        progress()
        downloadFile(files: ArraySlice(DownloadProvider.newFiles), group: group)
        group.notify(queue: .global()) {
            result(self.downloadDesciption, self.downloadStatus)
        }
    }
    
    func progress() {
        let percent = Float(downloadedFiles) / Float(DownloadProvider.newFiles.count)
        DispatchQueue.main.async {
            SVProgressHUD.showProgress(percent, status: "Скачано \(self.downloadedFiles) из \(DownloadProvider.newFiles.count) \(DownloadProvider.newFiles.count.filesRightWord)")
        }
    }

    func clearNonExistingFiles(files: [Files.FileMetadata]) {
        self.dropboxApi.listFilesWith(path: "") { (objects, error) in
            if let objectsOk = objects {
                let onlineFiles = Set(objectsOk.filter { $0 is Files.FileMetadata }.map { return $0.pathDisplay! })
                let offlineFiles = Set(self.fileProvider.getAllFiles())
                let toDelete = offlineFiles.intersection(onlineFiles)
                toDelete.forEach({ (pathWithoutRoot) in
                    self.fileProvider.delete(pathWithoutRoot: pathWithoutRoot)
                })
            }
        }
    }
    
    func isInternetConnectionActive() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
    }
}

