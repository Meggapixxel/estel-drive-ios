import UIKit

class FileFolder: NSObject {
    
    var name: String!
    var size: String?
    var date: String?
    var image: UIImage?
    var selfFolder: Folder?
    var selfFile: File?
    var root: Folder?
    
    init(name: String, selfFolder: Folder) {
        super.init()
        self.name = name
        self.selfFolder = selfFolder
        self.image = #imageLiteral(resourceName: "Folder")
    }
    
    init(name: String, date: Date, size: String, selfFile: File) {
        super.init()
        self.name = name
        self.date = date.ddMMyyyy
        self.size = size
        self.selfFile = selfFile
        self.image = imageForExtension()
    }
    
    init(root: Folder) {
        super.init()
        self.name = "..."
        self.root = root
    }
    
    func imageForExtension() -> UIImage {
        switch name.fileExt {
        case "pdf":
            return #imageLiteral(resourceName: "pdf")
        case "png":
            return #imageLiteral(resourceName: "png")
        case "jpeg", "jpg":
            return #imageLiteral(resourceName: "jpg")
        case "doc", "docx":
            return #imageLiteral(resourceName: "word")
        case "xls", "xlsx":
            return #imageLiteral(resourceName: "excel")
        case "ppt", "pptx":
            return #imageLiteral(resourceName: "powerpoint")
        case "mov", "avi", "mp4":
            return #imageLiteral(resourceName: "video")
        default:
            return #imageLiteral(resourceName: "File")
        }
    }
}
