import RealmSwift

class LogProvider: NSObject {

    private var realm: Realm!
    
    override init() {
        realm = try! Realm()
    }

    func writeLog(logFile: String, logDate: Date = Date(), logStatus: Bool = false) {
        let logItem = LogItem()
        logItem.logFile = logFile
        logItem.logDate = logDate
        logItem.logStatus = logStatus
        try! realm?.write {
            realm.add(logItem)
        }
    }
    
    func clearExpiredLogs() {
        let limitDate = Calendar.current.date(byAdding: .month, value: -1, to: Date())!
        let deleteLogs = realm.objects(LogItem.self).filter("logDate <= %@", limitDate)
        try! realm?.write {
            realm.delete(deleteLogs)
        }
    }
    
    func updateLogs() -> [LogItem]{
        clearExpiredLogs()
        return realm.objects(LogItem.self).map{ return $0 }
    }
}
