import UIKit
import FacebookCore
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyDropbox
import SVProgressHUD
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let options: UNAuthorizationOptions = [.alert, .sound]
    var backgroundTask: UIBackgroundTaskIdentifier!
    let downloadProvider = DownloadProvider()
    var download = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        prepareNotification()
        DropboxClientsManager.setupWithAppKey("pke5yjnwrj2bgik")
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if download {
            singleNotification(title: "Скачивание", body: "Загрузка файлов в фоновом режиме")
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let sourceApplication: String? = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: sourceApplication, annotation: nil)
    }
    
    func prepareNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("requestAuthorization error")
            }
        }
        // need check settings also
    }

    func scheduleNotification(at date: Date) {
        
        cancelSchedule()
        
        let content = UNMutableNotificationContent()
        content.title = "Напоминание"
        content.body = "Проверьте наличие новых файлов"
        content.sound = UNNotificationSound.default()
        
        let components = Calendar.current.dateComponents([.hour, .minute, .second], from: date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
        
        let request = UNNotificationRequest(identifier: "RepeatableNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            if let error = error { print("Uh oh! We had an error: \(error)") }
        })
    }
    
    func singleNotification(title: String, body: String) {
        
        clearShownNotifications()
        
        let content = UNMutableNotificationContent()
            content.title = title
            content.body = body
        
        let components = Calendar.current.dateComponents([.nanosecond], from: Date())
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
     
        let request = UNNotificationRequest(identifier: "SingleNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            if let error = error { print("Uh oh! We had an error: \(error)") }
        })
    }
    
    func cancelSchedule() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    func clearShownNotifications() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    func doBackgroundTask() {
        download = true
        SVProgressHUD.show()
        backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            SVProgressHUD.showError(withStatus: "Достигнуто максимальное время загрузки файлов")
            self.singleNotification(title: "Результат скачивания", body: "Достигнуто максимальное время загрузки файлов")
            self.endBackgroundTask()
        })
        
        DispatchQueue.global().async {
            self.downloadProvider.downloadNewFilesSync(result: { (str, bl) in
                self.download = false
                self.singleNotification(title: "Результат скачивания", body: str)
                DispatchQueue.main.async {
                    bl ? SVProgressHUD.showSuccess(withStatus: str) : SVProgressHUD.showError(withStatus: str)
                }
                self.endBackgroundTask()
            })
        }
    }
    
    func endBackgroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
}
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping(UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
}

