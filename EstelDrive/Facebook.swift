import FacebookCore

protocol FBCallback {
    func didGotFBinfo(info: MyProfileRequest.Response?)
    func didGotFBAccess(status: Bool?)
}

struct MyProfileRequest: GraphRequestProtocol {
    struct Response: GraphResponseProtocol {
        fileprivate let rawResponse: Any?
        
        public init(rawResponse: Any?) {
            self.rawResponse = rawResponse
        }
        
        public var dictionaryValue: [String : Any]? {
            return rawResponse as? [String : Any]
        }
        
        public var arrayValue: [Any]? {
            return rawResponse as? [Any]
        }
        
        public var stringValue: String? {
            return rawResponse as? String
        }
    }
    
    var graphPath = "/me"
    var parameters: [String : Any]? = ["fields": "id, name"]
    var accessToken = AccessToken.current
    var httpMethod: GraphRequestHTTPMethod = .GET
    var apiVersion: GraphAPIVersion = .defaultVersion
}

class Facebook {
    
    var callback: FBCallback?
    
    init() { }
    
    init(callback: FBCallback) {
        self.callback = callback
    }
    
    func getFBinfo() {
        let connection = GraphRequestConnection()
        connection.add(MyProfileRequest()) { response, result in
            switch result {
            case .success(let response):
                self.callback?.didGotFBinfo(info: response)
            case .failed(let error):
                print("Custom Graph Request Failed: \(error)")
                self.callback?.didGotFBinfo(info: nil)
            }
        }
        connection.start()
    }
    
    func checkUserInGroupWith(id: String?) {
        if let userId = id {
            let connection = GraphRequestConnection()
            connection.add(GraphRequest(graphPath: "/1716499648632464/members?limit=300&offset=0", parameters: ["fields": "id"])) { httpResponse, result in
                switch result {
                case .success(let response):
                    var array = [String]()
                    if let dict = response.dictionaryValue {
                        if let data = dict["data"] as? [Dictionary<String, String>] {
                            array = data.map { return $0["id"]! }
                        }
                    }
                    DispatchQueue.main.async {
                        self.callback?.didGotFBAccess(status: array.contains(userId))
                    }
                case .failed:
                    DispatchQueue.main.async {
                        self.callback?.didGotFBAccess(status: nil)
                    }
                }
            }
            connection.start()
        } else {
            DispatchQueue.main.async {
                self.callback?.didGotFBAccess(status: false)
            }
        }
    }
    
    func checkUserInGroupWith(id: String?, status: @escaping ((Bool?) -> Void)) {
        if let userId = id {
            let connection = GraphRequestConnection()
            connection.add(GraphRequest(graphPath: "/1716499648632464/members?limit=300&offset=0", parameters: ["fields": "id"])) { httpResponse, result in
                switch result {
                case .success(let response):
                    var array = [String]()
                    if let dict = response.dictionaryValue {
                        if let data = dict["data"] as? [Dictionary<String, String>] {
                            array = data.map { return $0["id"]! }
                        }
                    }
                    status(array.contains(userId))
                case .failed(let error):
                    print("Graph Request Failed: \(error)")
                    status(nil)
                }
            }
            connection.start()
        } else {
            status(nil)
        }
    }
}
