import UIKit

class EventLogCell: UITableViewCell {

    @IBOutlet weak var logFile: UILabel!
    @IBOutlet weak var logDate: UILabel!
    @IBOutlet weak var logStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
